package com.example.structure.num25;

import com.example.dto.ListNode;

public class Solution {

    public static void main(String[] args) {
        ListNode head6 = new ListNode(6);
        ListNode head5 = new ListNode(5, head6);
        ListNode head4 = new ListNode(4, head5);
        ListNode head3 = new ListNode(3, head4);
        ListNode head2 = new ListNode(2, head3);
        ListNode head1 = new ListNode(1, head2);
        System.out.println(head1.toString());

        System.out.println(reverseNode(head1).toString());


    }

    public ListNode reverseKGroup(ListNode head, int k) {
        return null;
    }

    public static ListNode reverseNode(ListNode head) {
        if(head.getNext() == null) {
            return head;
        }

        ListNode newHead = reverseNode(head.getNext());

        head.getNext().setNext(head);
        head.setNext(null);
        return newHead;

    }

}
