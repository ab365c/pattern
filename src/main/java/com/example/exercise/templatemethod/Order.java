package com.example.exercise.templatemethod;

/**
 * @author Administrator
 * @description
 * @date 2022/2/25 14:44
 */
public class Order {
    private Long goodsId;
    private String goodsName;
    private Integer unitPrice;
    private Integer totalPrice;
    private Integer count;
    private String payment;
}
