package com.example.exercise.templatemethod;

/**
 * @author Administrator
 * @description
 * @date 2022/2/25 14:11
 */
public abstract class OrderCenter {

    /**
     * 主方法，执行模板方法的所有
     */
    public void mainMethod() {

    }

    public void createOrder() {
        // 生成订单;
    }

    /**
     * 支付，选择不同的支付策略：微信，支付宝，银联等
     */
    public abstract void payOrder();

    /**
     * 发货，选择不同的发货方式：京东，顺丰，邮政等
     */
    public abstract void sendGoods();

    public void finishOrder() {

    }
}
