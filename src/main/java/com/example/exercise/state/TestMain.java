package com.example.exercise.state;

/**
 * @author Administrator
 * @description
 * @date 2022/3/1 16:24
 */
public class TestMain {
    public static void main(String[] args) {
        State state = new State();
        Context context = new Context(state);

        state.setValue("state1");
        context.method();

        state.setValue("state2");
        context.method();
    }
}
