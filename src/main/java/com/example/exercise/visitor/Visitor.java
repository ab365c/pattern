package com.example.exercise.visitor;

public interface Visitor {
    void visit(Subject subject);
}
