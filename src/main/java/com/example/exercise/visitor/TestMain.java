package com.example.exercise.visitor;

/**
 * @author Administrator
 * @description
 * @date 2022/3/3 10:10
 */
public class TestMain {
    public static void main(String[] args) {
        Visitor visitor = new MyVisitor();
        Subject subject = new MySubject();
        subject.accept(visitor);
    }
}
