package com.example.exercise.visitor;

public interface Subject {

    void accept(Visitor visitor);

    String getSubject();
}
