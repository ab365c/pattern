package com.example.exercise.visitor;

/**
 * @author Administrator
 * @description
 * @date 2022/3/3 10:08
 */
public class MyVisitor implements Visitor {

    @Override
    public void visit(Subject subject) {
        System.out.println("visit the subject:" + subject.getSubject());
    }
}
