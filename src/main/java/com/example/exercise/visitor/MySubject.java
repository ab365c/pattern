package com.example.exercise.visitor;

/**
 * @author Administrator
 * @description
 * @date 2022/3/3 10:09
 */
public class MySubject implements Subject {
    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String getSubject() {
        return "love";
    }
}
