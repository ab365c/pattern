package com.example.exercise.adapter.obj;

/**
 * @author Administrator
 * @description TODO
 * @date 2021/12/30 14:27
 */
public class Wrapper implements Targetable{
    private Source source;

    public Wrapper(Source source) {
        this.source = source;
    }

    @Override
    public void method1() {
        source.method1();
    }

    @Override
    public void method2() {
        System.out.println("This is adapter method.");
    }
}
