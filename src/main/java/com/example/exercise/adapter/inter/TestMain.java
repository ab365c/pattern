package com.example.exercise.adapter.inter;

/**
 * @author Administrator
 * @description TODO
 * @date 2021/12/30 15:05
 */
public class TestMain {
    public static void main(String[] args) {
        Sourceable source1 = new SourceSub1();
        Sourceable source2 = new SourceSub2();

        source1.method1();
        source1.method2();
        source2.method1();
        source2.method2();
    }
}
