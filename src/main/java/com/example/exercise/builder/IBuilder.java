package com.example.exercise.builder;


public interface IBuilder {
    Product build();
}
