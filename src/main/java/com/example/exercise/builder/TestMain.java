package com.example.exercise.builder;

/**
 * @author Administrator
 * @description 建造者模式 创建型模式
 * 适用于创建对象需要很多步骤，但是步骤的顺序不一定固定
 * 如果一个对象有非常复杂的内部结构（很多属性）
 * 把复杂对象的创建和使用分离
 * @date 2021/12/17 15:46
 */
public class TestMain {
    public static void main(String[] args) {
        // 构建sql语句
        String date = null;
        System.out.println(String.format("未查询到数据，日期:%s", date));

    }
}
