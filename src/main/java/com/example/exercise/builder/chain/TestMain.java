package com.example.exercise.builder.chain;

/**
 * @author Administrator
 * @description TODO
 * @date 2021/12/23 13:54
 */
public class TestMain {
    public static void main(String[] args) {
        CourseBuilder builder = new CourseBuilder();
        builder.addName("设计模式")
                .addPpt("PPT课件")
                .addVideo("录播视频");
        System.out.println(builder.build());
    }
}
