package com.example.exercise.builder.chain;

/**
 * @author Administrator
 * @description TODO
 * @date 2021/12/23 13:51
 */
public class CourseBuilder {

    private Course course = new Course();

    public CourseBuilder addName(String name) {
        course.setName(name);
        return this;
    }
    public CourseBuilder addPpt(String ppt) {
        course.setPpt(ppt);
        return this;
    }
    public CourseBuilder addNote(String note) {
        course.setNote(note);
        return this;
    }
    public CourseBuilder addVideo(String video) {
        course.setVideo(video);
        return this;
    }
    public CourseBuilder addHomework(String homework) {
        course.setHomework(homework);
        return this;
    }


    public Course build(){
        return course;
    }



}
