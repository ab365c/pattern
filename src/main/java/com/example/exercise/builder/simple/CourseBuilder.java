package com.example.exercise.builder.simple;

/**
 * @author Administrator
 * @description TODO
 * @date 2021/12/23 13:51
 */
public class CourseBuilder {

    private Course course = new Course();

    public void addName(String name) {
        course.setName(name);
    }
    public void addPpt(String ppt) {
        course.setPpt(ppt);
    }
    public void addNote(String note) {
        course.setNote(note);
    }
    public void addVideo(String video) {
        course.setVideo(video);
    }
    public void addHomework(String homework) {
        course.setHomework(homework);
    }


    public Course build(){
        return course;
    }



}
