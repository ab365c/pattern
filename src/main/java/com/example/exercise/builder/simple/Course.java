package com.example.exercise.builder.simple;

import lombok.Data;
import lombok.ToString;

/**
 * @author Administrator
 * @description TODO
 * @date 2021/12/23 13:49
 */
@Data
@ToString
public class Course {
    private String name;
    private String ppt;
    private String video;
    private String note;
    private String homework;

}
