package com.example.exercise.prototype;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Administrator
 * @description 原型模式
 * 定义：指原型实例指定创建对象的种类，并且通过拷贝这些原型，创建新的对象
 * 调用者不需要知道任何创建细节，不调用构造函数
 * 原型模式的使用场景：
 * 1. 类初始化消耗资源较多
 * 2. new产生的一个对象需要非常繁琐的过程（数据准备、访问权限等）
 * 3. 构造函数比较复杂
 * 4. 循环体中生产大连对象时
 * @date 2021/12/17 15:46
 */
public class TestMain {
    public static void main(String[] args) {
        // 浅克隆，深克隆
        ConcretePrototype prototype = new ConcretePrototype();
        prototype.setAge(10);
        prototype.setName("Tom");
        List<String> hobbies = new ArrayList<>();
        hobbies.add("书法");
        hobbies.add("美术");
        prototype.setHobbies(hobbies);

//        ConcretePrototype cloneType = prototype.clone();
        ConcretePrototype cloneType = prototype.deepClone();
        cloneType.getHobbies().add("技术控");
        System.out.println(prototype);
        System.out.println(cloneType);
        System.out.println(prototype == cloneType);
        // 原型对象和克隆对象，属性使用的是同一个对象
        System.out.println("原型对象的爱好："+prototype.getHobbies());
        System.out.println("克隆对象的爱好："+cloneType.getHobbies());
        System.out.println(prototype.getHobbies() == cloneType.getHobbies());


    }
}
