package com.example.exercise.explain;

/**
 * @author Administrator
 * @description
 * @date 2022/3/3 16:06
 */
public class Minus implements Expression {
    private Context context;

    public Minus(Context context) {
        this.context = context;
    }


    @Override
    public int interpret() {
        return context.getNum1() - context.getNum2();
    }
}
