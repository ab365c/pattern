package com.example.exercise.explain;

/**
 * @author Administrator
 * @description
 * @date 2022/3/3 16:04
 */
public interface Expression {
    int interpret();
}
