package com.example.exercise.explain;

/**
 * @author Administrator
 * @description
 * @date 2022/3/3 16:03
 */
public class TestMain {
    public static void main(String[] args) {
        int result = new Minus(new Context(new Plus(new Context(2, 9)).interpret(), 8)).interpret();
        System.out.println(result);
    }
}
