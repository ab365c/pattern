package com.example.exercise.explain;

/**
 * @author Administrator
 * @description
 * @date 2022/3/3 16:05
 */
public class Plus implements Expression {
    private Context context;

    public Plus(Context context) {
        this.context = context;
    }

    @Override
    public int interpret() {
        return context.getNum1() + context.getNum2();
    }
}
