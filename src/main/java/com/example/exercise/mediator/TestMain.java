package com.example.exercise.mediator;

/**
 * @author Administrator
 * @description
 * @date 2022/3/3 15:49
 */
public class TestMain {
    public static void main(String[] args) {
        Mediator mediator = new MyMediator();
        mediator.createMediator();
        mediator.workAll();
    }
}
