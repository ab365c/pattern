package com.example.exercise.mediator;

public interface Mediator {
    void createMediator();

    void workAll();

}
