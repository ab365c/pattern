package com.example.exercise.mediator;

/**
 * @author Administrator
 * @description
 * @date 2022/3/3 15:47
 */
public class User1 extends User {

    public User1(Mediator mediator) {
        super(mediator);
    }

    @Override
    public void work() {
        System.out.println("user1 exe!");
    }
}
