package com.example.exercise.mediator;

/**
 * @author Administrator
 * @description
 * @date 2022/3/3 15:47
 */
public class User2 extends User {

    public User2(Mediator mediator) {
        super(mediator);
    }

    @Override
    public void work() {
        System.out.println("user2 exe!");
    }
}
