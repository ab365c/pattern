package com.example.exercise.mediator;

/**
 * @author Administrator
 * @description
 * @date 2022/3/3 15:46
 */
public abstract class User {
    private Mediator mediator;

    public User(Mediator mediator) {
        this.mediator = mediator;
    }

    public Mediator getMediator() {
        return mediator;
    }

    public abstract void work();
}
