package com.example.exercise.mediator;

/**
 * @author Administrator
 * @description
 * @date 2022/3/3 15:45
 */
public class MyMediator implements Mediator {

    private User user01;
    private User user02;

    public User getUser01() {
        return user01;
    }

    public User getUser02() {
        return user02;
    }

    @Override
    public void createMediator() {
        user01 = new User1(this);
        user02 = new User2(this);
    }

    @Override
    public void workAll() {
        user01.work();
        user02.work();
    }
}
