package com.example.exercise.decorator;

/**
 * @author Administrator
 * @description TODO
 * @date 2021/12/30 15:43
 */
public class Source implements Sourceable{
    @Override
    public void method() {
        System.out.println("This is source method.");
    }
}
