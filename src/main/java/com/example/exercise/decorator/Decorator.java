package com.example.exercise.decorator;

/**
 * @author Administrator
 * @description TODO
 * @date 2021/12/30 15:44
 */
public class Decorator implements Sourceable{

    private Source source;

    public Decorator(Source source) {
        this.source = source;
    }

    @Override
    public void method() {
        source.method();
        System.out.println("This is decorator method.");
    }
}
