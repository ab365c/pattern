package com.example.exercise.factory.abs;

/**
 * @author Administrator
 * @description TODO
 * @date 2021/12/16 14:07
 */
public class JavaCourseFactory extends CourseFactory{
    @Override
    protected INote createNode() {
        super.init();
        return new JavaNote();
    }

    @Override
    protected IVideo createVideo() {
        super.init();
        return new JavaVideo();
    }
}
