package com.example.exercise.factory.method;

import com.example.exercise.factory.Food;
import com.example.exercise.factory.FoodFactory;
import com.example.exercise.factory.Hamburger;

/**
 * @author Administrator
 * @description TODO
 * @date 2021/12/14 11:01
 */
public class HamburgerFactory implements FoodFactory {
    @Override
    public Food makeFood() {
        return new Hamburger();
    }
}
