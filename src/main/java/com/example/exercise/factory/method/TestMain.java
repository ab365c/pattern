package com.example.exercise.factory.method;

import com.example.exercise.factory.FoodFactory;

/**
 * @author Administrator
 * @description TODO
 * @date 2021/12/14 11:03
 */
public class TestMain {
    public static void main(String[] args) {
        FoodFactory hamburger = new HamburgerFactory();
        System.out.println(hamburger.makeFood().getName());

        FoodFactory frenchFriesFactory = new FrenchFriesFactory();
        System.out.println(frenchFriesFactory.makeFood().getName());

        FoodFactory steakFactory = new SteakFactory();
        System.out.println(steakFactory.makeFood().getName());
    }
}
