package com.example.exercise.factory.method;

import com.example.exercise.factory.Food;
import com.example.exercise.factory.FoodFactory;
import com.example.exercise.factory.Steak;

/**
 * @author Administrator
 * @description TODO
 * @date 2021/12/14 11:02
 */
public class SteakFactory implements FoodFactory {
    @Override
    public Food makeFood() {
        return new Steak();
    }
}
