package com.example.exercise.factory;

/**
 * @author Administrator
 * @description TODO
 * @date 2021/12/15 17:25
 */

public abstract class Meat extends BaseFood{

    public Meat(String name, Integer price) {
        super(name, price);
    }
}
