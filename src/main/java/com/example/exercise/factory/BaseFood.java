package com.example.exercise.factory;

import com.example.exercise.factory.Food;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Administrator
 * @description TODO
 * @date 2021/12/14 10:21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseFood implements Food {

    private String name;

    private Integer price;



}
