package com.example.exercise.factory;

public interface FoodFactory {
    Food makeFood();
}
