package com.example.exercise.factory;

public interface Food {
    String getName();
    Integer getPrice();

}
