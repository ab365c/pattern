package com.example.exercise.factory;

/**
 * @author Administrator
 * @description TODO
 * @date 2021/12/15 17:26
 */
public abstract class Drink extends BaseFood{

    public Drink(String name, Integer price) {
        super(name, price);
    }
}
