package com.example.exercise.factory.simple;

import com.example.exercise.factory.Food;
import com.example.exercise.factory.FrenchFries;
import com.example.exercise.factory.Hamburger;
import com.example.exercise.factory.Steak;

/**
 * @author Administrator
 * @description TODO
 * @date 2021/12/14 10:13
 */
public class FoodFactoryImpl {

//    public Food makeFood(String foodType) {
//
//        Food food = null;
//        switch (foodType) {
//            case "hamburger":
//                food = new Hamburger();
//                break;
//            case "frenchFries":
//                food = new FrenchFries();
//                break;
//            case "steak":
//                food = new Steak();
//                break;
//            default:
//                food = null;
//        }
//        return food;
//    }

    public Food makeFood(Class<? extends Food> clazz) {
        Food food = null;
        if (clazz != null) {
            try {
                food = clazz.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return food;
    }
}
