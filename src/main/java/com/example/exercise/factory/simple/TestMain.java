package com.example.exercise.factory.simple;

import com.example.exercise.factory.FrenchFries;
import com.example.exercise.factory.Hamburger;
import com.example.exercise.factory.Steak;

/**
 * @author Administrator
 * @description 简单工厂模式，适用于对象类型较少的情况
 * 优点：
 * 缺点：违背了单一职责原则，可以创建多种不同类型的对象，当新增产品族的时候，每次都需要修改工厂类的判断逻辑，违背开闭原则，不易扩展
 * 用反射优化后的简单工厂模式，再不新增产品族的时候，不违反开闭原则
 * @date 2021/12/14 10:50
 */
public class TestMain {

    public static void main(String[] args) {
        FoodFactoryImpl foodFactory = new FoodFactoryImpl();

//        System.out.println(foodFactory.makeFood("hamburger").getName());
//        System.out.println(foodFactory.makeFood("frenchFries").getName());
//        System.out.println(foodFactory.makeFood("steak").getName());
        System.out.println(foodFactory.makeFood(Hamburger.class).getName());
        System.out.println(foodFactory.makeFood(FrenchFries.class).getName());
        System.out.println(foodFactory.makeFood(Steak.class).getName());
    }
}
