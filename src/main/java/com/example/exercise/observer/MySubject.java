package com.example.exercise.observer;

/**
 * @author Administrator
 * @description
 * @date 2022/2/25 17:03
 */
public class MySubject extends AbstractSubject {
    @Override
    public void operate() {
        System.out.println("update self");
        notifyObservers();
    }
}
