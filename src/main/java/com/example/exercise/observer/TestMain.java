package com.example.exercise.observer;

/**
 * @author Administrator
 * @description 观察者模式
 * @date 2022/2/25 14:18
 */
public class TestMain {
    public static void main(String[] args) {
        Subject sub = new MySubject();
        sub.add(new Observer1());
        sub.add(new Observer2());
        sub.operate();
    }
}
