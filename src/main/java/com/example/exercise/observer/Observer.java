package com.example.exercise.observer;

public interface Observer {
    void update();
}
