package com.example.exercise.observer;

/**
 * @author Administrator
 * @description
 * @date 2022/2/25 16:58
 */
public class Observer2 implements Observer {
    @Override
    public void update() {
        System.out.println("Observer2 has received");
    }
}
