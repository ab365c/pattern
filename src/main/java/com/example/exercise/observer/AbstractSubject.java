package com.example.exercise.observer;

import java.util.Enumeration;
import java.util.Vector;

/**
 * @author Administrator
 * @description
 * @date 2022/2/25 17:01
 */
public abstract class AbstractSubject implements Subject {
    private Vector<Observer> vector = new Vector<>();

    @Override
    public void add(Observer observer) {
        vector.add(observer);
    }

    @Override
    public void del(Observer observer) {
        vector.remove(observer);
    }

    @Override
    public void notifyObservers() {
        Enumeration<Observer> elements = vector.elements();
        while (elements.hasMoreElements()) {
            elements.nextElement().update();
        }
    }

    @Override
    public abstract void operate();
}
