package com.example.exercise.observer;

public interface Subject {
    void add(Observer observer);

    void del(Observer observer);

    void notifyObservers();

    void operate();
}
