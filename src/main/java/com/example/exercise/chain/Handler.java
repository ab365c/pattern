package com.example.exercise.chain;

public interface Handler {
    void operate();
}
