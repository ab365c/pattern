package com.example.exercise.chain;

/**
 * @author Administrator
 * @description
 * @date 2022/3/1 15:06
 */
public abstract class AbstractHandler {
    private Handler handler;

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }
}
