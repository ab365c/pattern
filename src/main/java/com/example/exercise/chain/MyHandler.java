package com.example.exercise.chain;

/**
 * @author Administrator
 * @description
 * @date 2022/3/1 15:07
 */
public class MyHandler extends AbstractHandler implements Handler {

    private String name;

    public MyHandler(String name) {
        this.name = name;
    }

    @Override
    public void operate() {
        System.out.println(name + " deal!");
        if (getHandler() != null) {
            getHandler().operate();
        }
    }
}
