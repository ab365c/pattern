package com.example.exercise.memento;

/**
 * @author Administrator
 * @description
 * @date 2022/3/1 16:06
 */
public class Memento {
    private String value;

    public Memento(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
