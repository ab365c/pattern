package com.example.exercise.memento;

/**
 * @author Administrator
 * @description
 * @date 2022/3/1 16:12
 */
public class Storage {
    private Memento memento;

    public Storage(Memento memento) {
        this.memento = memento;
    }

    public Memento getMemento() {
        return memento;
    }

    public void setMemento(Memento memento) {
        this.memento = memento;
    }
}
