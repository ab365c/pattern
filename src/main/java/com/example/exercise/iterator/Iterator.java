package com.example.exercise.iterator;

public interface Iterator {

    Object privious();

    Object next();

    boolean hasNext();

    Object first();
}
