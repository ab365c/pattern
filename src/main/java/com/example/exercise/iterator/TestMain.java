package com.example.exercise.iterator;

/**
 * @author Administrator
 * @description
 * @date 2022/3/1 14:21
 */
public class TestMain {
    public static void main(String[] args) {
        Collection collection = new MyCollection();
        Iterator iterator = collection.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
