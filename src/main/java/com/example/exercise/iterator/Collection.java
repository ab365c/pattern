package com.example.exercise.iterator;

public interface Collection {
    Iterator iterator();

    Object get(int i);

    int size();
}
