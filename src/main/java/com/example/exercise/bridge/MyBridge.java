package com.example.exercise.bridge;

/**
 * @author Administrator
 * @description TODO
 * @date 2021/12/30 17:27
 */
public class MyBridge extends Bridge {
    @Override
    public void method() {
        getSourceable().method();
    }
}
