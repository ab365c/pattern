package com.example.exercise.bridge;

/**
 * @author Administrator
 * @description TODO
 * @date 2021/12/30 17:26
 */
public abstract class Bridge {
    private Sourceable sourceable;
    public void method(){
        sourceable.method();
    }

    public Sourceable getSourceable() {
        return sourceable;
    }

    public void setSourceable(Sourceable sourceable) {
        this.sourceable = sourceable;
    }
}
