package com.example.exercise.bridge;

/**
 * @author Administrator
 * @description TODO
 * @date 2021/12/30 17:28
 */
public class TestMain {
    public static void main(String[] args) {

        Bridge bridge = new MyBridge();
        Sourceable source1 = new SourceSub1();
        bridge.setSourceable(source1);
        bridge.method();

        Sourceable source2 = new SourceSub2();
        bridge.setSourceable(source2);
        bridge.method();
    }
}
