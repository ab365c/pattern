package com.example.exercise.strategy;

/**
 * @author Administrator
 * @description
 * @date 2022/2/25 9:53
 */
public class Plus extends AbstractCalculator implements ICalculator {
    @Override
    public int calculate(String exp) {
        int[] arrayInt = split(exp, "-");
        return arrayInt[0] + arrayInt[1];
    }
}
