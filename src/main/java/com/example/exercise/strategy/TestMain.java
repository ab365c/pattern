package com.example.exercise.strategy;

/**
 * @author Administrator
 * @description 策略模式，定义了一系列算法，并将每个算法封装起来，使他们可以相互替换，
 * 且算法的变化不会影响到使用算法的用户。
 * 需要设计一个接口，为一系列实现类提供统一的方法，多个实现类实现该接口，
 * 设计一个抽象类（可有可无，属于辅助类），提供辅助函数
 * 场景举例：多种登录方式，多种支付方式，
 * @date 2022/2/25 9:48
 */
public class TestMain {
    public static void main(String[] args) {
        String exp = "2-8";
        ICalculator cal = new Plus();
        int add = cal.calculate(exp);
        System.out.println("add result:" + add);
        cal = new Minus();
        int minus = cal.calculate(exp);
        System.out.println("minus result:" + minus);
        cal = new Multiply();
        int multiply = cal.calculate(exp);
        System.out.println("multiply result:" + multiply);
    }
}
