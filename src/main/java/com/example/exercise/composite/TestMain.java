package com.example.exercise.composite;

/**
 * @author Administrator
 * @description TODO
 * @date 2022/1/17 10:08
 */
public class TestMain {
    public static void main(String[] args) {
        Tree tree = new Tree("A");
        TreeNode nodeB = new TreeNode("B");
        TreeNode nodeC = new TreeNode("C");

        nodeB.add(nodeC);
        tree.getRoot().add(nodeB);
        System.out.println("build the tree finished!");
    }
}
