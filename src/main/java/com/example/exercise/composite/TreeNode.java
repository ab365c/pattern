package com.example.exercise.composite;

import lombok.Data;

import java.util.Enumeration;
import java.util.Vector;

/**
 * @author Administrator
 * @description TODO
 * @date 2022/1/17 10:09
 */
@Data
public class TreeNode {
    private String name;
    private TreeNode parent;
    private Vector<TreeNode> children = new Vector<>();

    public TreeNode(String name) {
        this.name = name;
    }

    /**
     * 添加孩子节点
     *
     * @param node
     */
    public void add(TreeNode node) {
        children.add(node);
    }

    /**
     * 删除孩子节点
     *
     * @param node
     */
    public void remove(TreeNode node) {
        children.remove(node);
    }

    /**
     * 获取孩子节点
     *
     * @return
     */
    public Enumeration<TreeNode> getChildren() {
        return children.elements();
    }
}
