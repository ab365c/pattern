package com.example.exercise.composite;

import lombok.Data;

/**
 * @author Administrator
 * @description TODO
 * @date 2022/1/17 10:14
 */

@Data
public class Tree {
    private TreeNode root;

    public Tree(String name) {
        this.root = new TreeNode(name);
    }
}
