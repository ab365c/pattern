package com.example.exercise.command;

/**
 * @author Administrator
 * @description
 * @date 2022/3/1 15:27
 */
public class TestMain {
    public static void main(String[] args) {
        Receiver receiver = new Receiver();
        Command command = new MyCommand(receiver);
        Invoker invoker = new Invoker(command);
        invoker.action();
    }
}
