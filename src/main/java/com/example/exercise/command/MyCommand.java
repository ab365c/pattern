package com.example.exercise.command;

/**
 * @author Administrator
 * @description
 * @date 2022/3/1 15:25
 */
public class MyCommand implements Command {
    private Receiver receiver;

    public MyCommand(Receiver receiver) {
        this.receiver = receiver;
    }

    @Override
    public void exe() {
        receiver.action();
    }
}
