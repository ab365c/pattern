package com.example.exercise.command;

/**
 * @author Administrator
 * @description
 * @date 2022/3/1 15:26
 */
public class Receiver {

    public void action() {
        System.out.println("command received!");
    }
}
