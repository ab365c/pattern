package com.example.exercise.command;

public interface Command {
    void exe();
}
