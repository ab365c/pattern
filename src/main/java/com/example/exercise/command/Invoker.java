package com.example.exercise.command;

/**
 * @author Administrator
 * @description
 * @date 2022/3/1 15:27
 */
public class Invoker {
    private Command command;

    public Invoker(Command command) {
        this.command = command;
    }

    public void action() {
        command.exe();
    }
}
