package com.example.exercise.flyweight;

public interface IFlyweight {
    void operation(String intrinsicState);
}
