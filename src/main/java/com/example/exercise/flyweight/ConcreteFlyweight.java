package com.example.exercise.flyweight;

/**
 * @author Administrator
 * @description
 * @date 2022/2/24 14:45
 */
public class ConcreteFlyweight implements IFlyweight {

    private String intrinsicState;

    @Override
    public void operation(String intrinsicState) {

    }
}
