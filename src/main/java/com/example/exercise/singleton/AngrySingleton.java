package com.example.exercise.singleton;

/**
 * @author Administrator
 * @description TODO
 * @date 2021/12/14 14:47
 */
public class AngrySingleton {

    private static AngrySingleton instance = new AngrySingleton();

    private AngrySingleton() {
    }

    public static AngrySingleton getInstance() {
        return instance;
    }
}
