package com.example.exercise.singleton;

/**
 * @author Administrator
 * @description TODO
 * @date 2021/12/14 14:59
 */
public class StaticSingleton {
    private static StaticSingleton instance;
    private StaticSingleton(){

    }

    public static StaticSingleton getInstance (){
        return StaticSingletonHolder.instance;
    }

    private static class StaticSingletonHolder{
        private static StaticSingleton instance = new StaticSingleton();
    }

}
