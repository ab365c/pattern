package com.example.exercise.singleton;

/**
 * @author Administrator
 * @description TODO
 * @date 2021/12/14 14:49
 */
public class LazySingleton {

    private static LazySingleton instance;

    private LazySingleton(){

    }

    public static LazySingleton getInstance() {
        if (instance == null) {
            instance = new LazySingleton();
        }
        return instance;
    }
}
