package com.example.all.factorymethod;

import com.example.all.bean.food.IFood;

/**
 * 工厂方法模式
 * <p>
 * 使用场景：不要知道产品类的名称，只需要知道工厂的名称就可以了，产品种类无法区分出产品族。
 * 优点：当以后需要增加新的产品时，直接增加一个工厂就可以了，而不是去修改原有的工厂
 * 缺点：开销增大，需要为每个产品创建一个工厂类，当产品数量变多的时候，会导致出现大量的工厂类
 * <p>
 * 第二章 出错
 * 张三的店开了一段时间，开始还不错，人们还是乐意光顾新店的，慢慢的就变得人少了，都说食品种类太少
 * 张三一个劲儿发愁，为了不大量增加投资还得满足要求，是在一个小作坊式的工厂里，生产多种食品。
 * 可是有一天啊，因为要新增生产鸡翅，修改出现了异常，导致什么东西都做不了了，引得顾客投诉，既要赔礼，还要赔偿。
 * 有了这次的教训呢，张三开始思考解决方案，最终决定，增加投资，将不通食物，分开生产。
 * 于是就有了下面的模式
 */
public class TestMain {
    public static void main(String[] args) {
        FrenchFriesFactory frenchFriesFactory = new FrenchFriesFactory();
        HamburgerFactory hamburgerFactory = new HamburgerFactory();
        SteakFactory steakFactory = new SteakFactory();
        ChickenWingFactory chickenWingFactory = new ChickenWingFactory();

        IFood frenchFries = frenchFriesFactory.makeFood();
        IFood hamburger = hamburgerFactory.makeFood();
        IFood steak = steakFactory.makeFood();
        IFood chickenWing = chickenWingFactory.makeFood();

        System.out.println(frenchFries.getName());
        System.out.println(hamburger.getName());
        System.out.println(steak.getName());
        System.out.println(chickenWing.getName());
    }
}
