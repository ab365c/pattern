package com.example.all.factorymethod;

import com.example.all.bean.food.ChickenWing;
import com.example.all.bean.food.IFood;
import com.example.all.bean.food.IFoodFactory;

public class ChickenWingFactory implements IFoodFactory {

    @Override
    public IFood makeFood() {
        return new ChickenWing();
    }
}
