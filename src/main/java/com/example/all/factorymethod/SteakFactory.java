package com.example.all.factorymethod;

import com.example.all.bean.food.IFood;
import com.example.all.bean.food.IFoodFactory;
import com.example.all.bean.food.Steak;

public class SteakFactory implements IFoodFactory {
    @Override
    public IFood makeFood() {
        return new Steak();
    }
}
