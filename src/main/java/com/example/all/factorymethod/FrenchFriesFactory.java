package com.example.all.factorymethod;

import com.example.all.bean.food.FrenchFries;
import com.example.all.bean.food.IFood;
import com.example.all.bean.food.IFoodFactory;


public class FrenchFriesFactory implements IFoodFactory {
    @Override
    public IFood makeFood() {
        return new FrenchFries();
    }
}
