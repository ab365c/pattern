package com.example.all.factorymethod;

import com.example.all.bean.food.Hamburger;
import com.example.all.bean.food.IFood;
import com.example.all.bean.food.IFoodFactory;

public class HamburgerFactory implements IFoodFactory {
    @Override
    public IFood makeFood() {
        return new Hamburger();
    }
}
