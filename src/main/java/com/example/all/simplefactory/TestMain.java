package com.example.all.simplefactory;

import com.example.all.bean.food.FrenchFries;
import com.example.all.bean.food.Hamburger;
import com.example.all.bean.food.Steak;

/**
 * 简单工厂模式
 * 使用场景：将创建产品的逻辑与其他业务逻辑解耦
 * 优点：可以批量操作
 * 缺点：违反开闭原则，出现需求改动时，不能屏蔽修改，有可能影响其他功能
 * <p>
 * 作为一名码农，对于设计模式一直没有深刻地认识，学过一遍又一遍，始终无法记牢各种设计模式，下次碰到了，依然说不出个所以然，虽然我们工作中有时会在不知不觉中使用，但是理解还不是很深刻，不能够随心所欲
 * 在此，结合自身的学习习惯，认为通过一个生活中比较容易理解的场景，来学习一个知识或者一个技能，通常能够更快更牢固的掌握，所以，谨借用张三之名，开始学习设计模式
 * <p>
 * 第一章 开店
 * 张三毕业了，一直没找到什么工作，待业在家总不是个曲子呀，可是能干点什么呢，
 * 想到自己平时爱吃汉堡牛排，就决定开一家快餐店，一是为了方便自己，二是为了养活自己。
 * 刚开始的时候，摊子比较小，创办了一间小的食品工厂，就决定做三样东西，汉堡、牛排、薯条，所有的食品都由这一个工厂负责
 */
public class TestMain {
    public static void main(String[] args) {
        FoodFactory factory = new FoodFactory();
        FrenchFries frenchFries = (FrenchFries) factory.makeFood(FrenchFries.class.getName());
        System.out.println(frenchFries.getName());
        Hamburger hamburger = (Hamburger) factory.makeFood(Hamburger.class.getName());
        System.out.println(hamburger.getName());
        Steak steak = (Steak) factory.makeFood(Steak.class.getName());
        System.out.println(steak.getName());
    }
}
