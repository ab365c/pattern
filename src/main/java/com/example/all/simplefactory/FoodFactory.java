package com.example.all.simplefactory;

import com.example.all.bean.food.FrenchFries;
import com.example.all.bean.food.Hamburger;
import com.example.all.bean.food.IFood;
import com.example.all.bean.food.Steak;
import org.apache.commons.lang3.StringUtils;

/**
 * 工厂类，创建食物
 */
public class FoodFactory {

    /**
     * 制作食物
     *
     * @param foodName
     * @return
     */
    public IFood makeFood(String foodName) {
        if (StringUtils.isBlank(foodName)) {
            throw new RuntimeException("请输入食物名称！");
        } else if (FrenchFries.class.getName().equals(foodName)) {
            return new FrenchFries();
        } else if (Hamburger.class.getName().equals(foodName)) {
            return new Hamburger();
        } else if (Steak.class.getName().equals(foodName)) {
            return new Steak();
        } else {
            throw new RuntimeException(String.format("工厂能力不足，无法制作该食物：%s"));
        }
    }
}
