package com.example.all.bean.food;


public interface IFoodFactory {
    IFood makeFood();
}
