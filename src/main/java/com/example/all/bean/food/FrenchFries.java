package com.example.all.bean.food;

import lombok.Data;

@Data
public class FrenchFries implements IFood {


    @Override
    public String getName() {
        return "FrenchFries";
    }
}
