package com.example.pattern.decorator;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.decorator.ShapeDecorator
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/15 19:27
 * @version:1.0
 */
public interface ShapeDecorator extends Shape {
    Shape shape = null;
    @Override
    void draw();
}
