package com.example.pattern.decorator;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.decorator.RedShapeDecorator
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/15 19:31
 * @version:1.0
 */
public class RedShapeDecorator implements ShapeDecorator {
    public Shape shape;

    public RedShapeDecorator(Shape shape) {
        this.shape = shape;
    }

    @Override
    public void draw() {
        shape.draw();
        setRedColor();

    }

    private void setRedColor(){
        System.out.println("Red Colors");
    }
}
