package com.example.pattern.decorator;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.decorator.Rectangle
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/15 19:25
 * @version:1.0
 */
public class Rectangle implements Shape {
    @Override
    public void draw() {
        System.out.println("Draw Rectangle");
    }
}
