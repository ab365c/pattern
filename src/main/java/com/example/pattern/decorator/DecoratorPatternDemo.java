package com.example.pattern.decorator;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.decorator.DecoratorPatternDemo
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/15 19:22
 * @version:1.0
 */
public class DecoratorPatternDemo {
    public static void main(String[] args) {
        CircleShape circleShape = new CircleShape();
        ShapeDecorator redCircle = new RedShapeDecorator(circleShape);
        circleShape.draw();
        redCircle.draw();
    }
}
