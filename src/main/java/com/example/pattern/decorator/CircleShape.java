package com.example.pattern.decorator;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.decorator.CircleShape
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/15 19:23
 * @version:1.0
 */
public class CircleShape implements Shape{

    @Override
    public void draw() {
        System.out.println("Draw Circle.");
    }
}
