package com.example.pattern.stream;

import lombok.Data;

import java.util.List;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.stream.Student
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/6/28 10:46
 * @version:1.0
 */
@Data
public class Student {

    private Long id;
    private String name;
    private Integer age;
    private Integer sex;

    public Student() {
    }

    public Student(Long id, String name, Integer age, Integer sex) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.sex = sex;
    }
}
