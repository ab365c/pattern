package com.example.pattern.strategy;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.strategy.OperationMultiply
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/21 13:29
 * @version:1.0
 */
public class OperationMultiply implements Strategy {
    @Override
    public int doOperation(int num1, int num2) {
        return num1 * num2;
    }
}
