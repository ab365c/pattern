package com.example.pattern.strategy;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.strategy.OperationAdd
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/21 13:27
 * @version:1.0
 */
public class OperationAdd implements Strategy {
    @Override
    public int doOperation(int num1, int num2) {
        return num1 + num2;
    }
}
