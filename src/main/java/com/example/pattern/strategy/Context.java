package com.example.pattern.strategy;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.strategy.Context
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/21 13:34
 * @version:1.0
 */
public class Context {
    private Strategy strategy;

    public Context(Strategy strategy) {
        this.strategy = strategy;
    }

    public int executeStrategy(int num1, int num2) {
        return strategy.doOperation(num1, num2);
    }
}
