package com.example.pattern.strategy;

/**
 * @description: 策略模式
 * @project: pattern
 * @name: com.example.pattern.strategy.StrategyPatternDemo
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/21 13:36
 * @version:1.0
 */
public class StrategyPatternDemo {
    public static void main(String[] args) {
        int result = 0;

        Context addContext = new Context(new OperationAdd());
        result = addContext.executeStrategy(8,3);
        System.out.println(result);

        Context subtractContext = new Context(new OperationSubtract());
        result = subtractContext.executeStrategy(8,3);
        System.out.println(result);

        Context multiplyContext = new Context(new OperationMultiply());
        result = multiplyContext.executeStrategy(8,3);
            System.out.println(result);
    }
}
