package com.example.pattern.strategy;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.strategy.Strategy
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/21 13:30
 * @version:1.0
 */
public interface Strategy {
    int doOperation(int num1, int num2);
}
