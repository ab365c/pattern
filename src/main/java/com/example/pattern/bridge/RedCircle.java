package com.example.pattern.bridge;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.bridge.RedCircle
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/17 13:56
 * @version:1.0
 */
public class RedCircle implements DrawApi{
    @Override
    public void drawCircle(int radius, int x, int y) {
        System.out.println("Drawing Circle[ color: red, radius: "
                + radius +", x: " +x+", "+ y +"]");
    }
}
