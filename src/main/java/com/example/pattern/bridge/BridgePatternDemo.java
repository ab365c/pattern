package com.example.pattern.bridge;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.bridge.BridgePatternDemo
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/17 13:55
 * @version:1.0
 */
public class BridgePatternDemo {
    public static void main(String[] args) {
        Shape redCircle = new Circle(new RedCircle(), 100, 100, 10);
        Shape greenCircle = new Circle(new GreenCircle(), 100, 100, 10);

        redCircle.draw();
        greenCircle.draw();
    }
}
