package com.example.pattern.bridge;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.bridge.Shape
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/17 13:58
 * @version:1.0
 */
public abstract class Shape {
    protected DrawApi drawApi;
    protected Shape(DrawApi drawApi){
        this.drawApi = drawApi;
    }

    public abstract void draw();
}
