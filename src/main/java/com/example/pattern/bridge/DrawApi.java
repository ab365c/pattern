package com.example.pattern.bridge;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.bridge.DrawApi
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/17 13:55
 * @version:1.0
 */
public interface DrawApi {
    void drawCircle(int radius, int x, int y);
}
