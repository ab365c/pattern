package com.example.pattern.prototype;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.prototype.CircleShape
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/6/29 20:05
 * @version:1.0
 */
public class CircleShape extends Shape {

    public CircleShape() {
        type = "Circle";
    }

    @Override
    void draw() {
        System.out.println("Inside Circle::draw() method.");
    }
}
