package com.example.pattern.prototype;

import lombok.Data;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.prototype.Shape
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/6/29 20:00
 * @version:1.0
 */
@Data
public abstract class Shape implements Cloneable {
    private String id;
    protected String type;

    /**
     * 画出图形
     */
    abstract void draw();

    @Override
    public Object clone() {
        Object clone = null;
        try {
            clone = super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return clone;
    }
}
