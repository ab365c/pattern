package com.example.pattern.prototype;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.prototype.Square
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/6/29 20:06
 * @version:1.0
 */
public class SquareShape extends Shape {

    public SquareShape() {
        type = "Square";
    }

    @Override
    void draw() {
        System.out.println("Inside Square::draw() method.");
    }
}
