package com.example.pattern.prototype;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.prototype.RectangleShape
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/6/29 20:04
 * @version:1.0
 */
public class RectangleShape extends Shape {
    public RectangleShape() {
        type = "Rectangle";
    }

    @Override
    void draw() {
        System.out.println("Inside Rectangle::draw() method.");
    }
}
