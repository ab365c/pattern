package com.example.pattern.prototype;


import java.util.Hashtable;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.prototype.ShapeCache
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/6/29 20:07
 * @version:1.0
 */
public class ShapeCache {

    private static Hashtable<String, Shape> shapeMap = new Hashtable<>();

    public static Shape getShape(String shapeId){
        Shape cacheShape = shapeMap.get(shapeId);
        return (Shape) cacheShape.clone();
    }

    public static void loadShape() {
        CircleShape circleShape = new CircleShape();
        circleShape.setId("1");
        shapeMap.put(circleShape.getId(), circleShape);

        SquareShape squareShape = new SquareShape();
        squareShape.setId("2");
        shapeMap.put(squareShape.getId(), squareShape);

        RectangleShape rectangleShape = new RectangleShape();
        rectangleShape.setId("3");
        shapeMap.put(rectangleShape.getId(), rectangleShape);
    }
}
