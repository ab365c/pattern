package com.example.pattern.prototype;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.prototype.PrototypePatternDemo
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/6/29 20:00
 * @version:1.0
 */
public class PrototypePatternDemo {
    public static void main(String[] args) {
        ShapeCache.loadShape();

        Shape shape01 = ShapeCache.getShape("1");
        System.out.println("Shape:" + shape01.getType());

        Shape shape02 = ShapeCache.getShape("2");
        System.out.println("Shape:" + shape02.getType());

        Shape shape03 = ShapeCache.getShape("3");
        System.out.println("Shape:" + shape03.getType());
    }
}
