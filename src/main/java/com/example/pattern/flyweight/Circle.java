package com.example.pattern.flyweight;

import lombok.Data;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.flyweight.Circle
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/16 14:23
 * @version:1.0
 */
@Data
public class Circle implements Shape {
    private int x;
    private int y;
    private int radius;
    private String color;

    public Circle(String color) {
        this.color = color;
        this.x = 3;
        this.y = 5;
        this.radius = 8;
    }

    @Override
    public void draw() {
        System.out.println("Circle: Draw() [Color : " + color
                +", x : " + x +", y :" + y +", radius :" + radius);
    }
}
