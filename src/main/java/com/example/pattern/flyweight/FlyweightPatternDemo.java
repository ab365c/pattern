package com.example.pattern.flyweight;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.flyweight.FlyweightPatternDemo
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/16 14:22
 * @version:1.0
 */
public class FlyweightPatternDemo {
    public static void main(String[] args) {
        ShapeFactory.getCircle("red").draw();
        ShapeFactory.getCircle("blue").draw();
        ShapeFactory.getCircle("yellow").draw();
        ShapeFactory.getCircle("red").draw();
        ShapeFactory.getCircle("red").draw();
        ShapeFactory.getCircle("blue").draw();
        ShapeFactory.getCircle("red").draw();
    }
}
