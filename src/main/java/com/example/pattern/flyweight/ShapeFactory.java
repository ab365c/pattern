package com.example.pattern.flyweight;

import java.util.HashMap;
import java.util.Map;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.flyweight.ShapeFactory
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/16 14:29
 * @version:1.0
 */
public class ShapeFactory {
    private static Map<String, Circle> circleMap = new HashMap<>(8);

    public static Circle getCircle(String color) {
        Circle circle = circleMap.get(color);
        if (circle == null) {
           circle = new Circle(color);
           circleMap.put(color, circle);
            System.out.println("Create circle of color: " + color);
        }
        return circle;
    }
}
