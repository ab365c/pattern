package com.example.pattern.flyweight;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.flyweight.Shape
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/16 14:22
 * @version:1.0
 */
public interface Shape {
    void draw();
}
