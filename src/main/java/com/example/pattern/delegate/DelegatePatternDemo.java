package com.example.pattern.delegate;

/**
 * @description:
 * 委派对象不知道任务如何处理，交给其他对象处理
 * 实现程序的解耦，运行阶段的行为处理的逻辑，注重委派规则
 * 场景1，老板，员工，老板给员工下达任务，员工把结果返回给老板
 * 场景2. 办理业务的时候，去找律师、公证等，委托他人去办理业务
 *
 * @project: pattern
 * @name: com.example.pattern.delegate.DelegatePatternDemo
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/17 14:35
 * @version:1.0
 */
public class DelegatePatternDemo {
    public static void main(String[] args) {
        LeaderA leaderA = new LeaderA();
        leaderA.executing("java");
        leaderA.executing("js");
        leaderA.executing("vue");
    }

}
