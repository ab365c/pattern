package com.example.pattern.delegate;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.delegate.EmployeeC
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/17 14:48
 * @version:1.0
 */
public class EmployeeC implements Employee {
    @Override
    public void executing(String task) {
        System.out.println("doing Android");
    }
}
