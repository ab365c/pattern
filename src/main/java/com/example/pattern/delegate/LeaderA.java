package com.example.pattern.delegate;

import java.sql.SQLOutput;
import java.util.HashMap;
import java.util.Map;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.delegate.Leader
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/17 14:41
 * @version:1.0
 */
public class LeaderA implements Employee{

    Map<String, Employee> map = new HashMap<>(8);

    private Employee delegate(String msg){
        map.put("java", new EmployeeA());
        map.put("js", new EmployeeB());
        map.put("android", new EmployeeC());

        if(map.keySet().contains(msg)){
            return map.get(msg);
        } else {
            System.out.println("委派任务出错！");
            return null;
        }
    }

    @Override
    public void executing(String task) {
        if(delegate(task)!=null){
            delegate(task).executing(task);
        }
    }
}
