package com.example.pattern.delegate;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.delegate.EmployeeA
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/17 14:41
 * @version:1.0
 */
public class EmployeeA implements Employee {
    @Override
    public void executing(String task) {
        System.out.println("doing java");
    }
}
