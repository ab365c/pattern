package com.example.pattern.delegate;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.delegate.Employee
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/17 14:42
 * @version:1.0
 */
public interface Employee {

    void executing(String task);
}
