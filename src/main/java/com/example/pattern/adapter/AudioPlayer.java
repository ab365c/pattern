package com.example.pattern.adapter;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.adapter.AuditPlayer
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/16 13:50
 * @version:1.0
 */
public class AudioPlayer implements MediaPlayer {
    MediaAdapter mediaAdapter;

    @Override
    public void play(String type, String fileName) {
        if ("mp3".equals(type)) {
            System.out.println("Play mp3: " + fileName);
        } else if ("mp4".equals(type) || "vlc".equals(type)) {
            mediaAdapter = new MediaAdapter();
            mediaAdapter.play(type, fileName);
        } else {
            System.out.println("Not support type!");
        }
    }
}
