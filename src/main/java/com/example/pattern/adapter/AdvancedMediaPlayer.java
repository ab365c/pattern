package com.example.pattern.adapter;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.adapter.AdvancedMediaPlayer
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/16 13:51
 * @version:1.0
 */
public interface AdvancedMediaPlayer {
    /**
     * 播放音乐
     */
    void playVlc(String fileName);

    void playMp4(String fileName);
}
