package com.example.pattern.adapter;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.adapter.AdapterPatternDemo
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/16 14:13
 * @version:1.0
 */
public class AdapterPatternDemo {
    public static void main(String[] args) {
        MediaPlayer mediaPlayer = new AudioPlayer();
        mediaPlayer.play("mp3", "mp3.file");
        mediaPlayer.play("mp4", "mp4.file");
        mediaPlayer.play("vlc", "vlc.file");
        mediaPlayer.play("avi", "avi.file");
    }
}
