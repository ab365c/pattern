package com.example.pattern.adapter;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.adapter.MediaPlayer
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/16 13:47
 * @version:1.0
 */
public interface MediaPlayer {
    /**
     * 播放音乐
     * @param type 类型
     * @param fileName 文件名
     */
    void play(String type, String fileName);
}
