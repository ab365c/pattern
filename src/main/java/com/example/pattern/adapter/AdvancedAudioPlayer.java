package com.example.pattern.adapter;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.adapter.AdvancedAudioPlayer
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/16 13:53
 * @version:1.0
 */
public class AdvancedAudioPlayer implements AdvancedMediaPlayer {
    @Override
    public void playVlc(String fileName) {
        System.out.println("Play vlc: " + fileName);
    }

    @Override
    public void playMp4(String fileName) {
        System.out.println("Play mp4: " + fileName);
    }
}
