package com.example.pattern.adapter;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.adapter.MediaAdapter
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/16 13:55
 * @version:1.0
 */
public class MediaAdapter implements MediaPlayer {

    AdvancedMediaPlayer advancedMediaPlayer;

    public MediaAdapter() {
        advancedMediaPlayer = new AdvancedAudioPlayer();
    }

    @Override
    public void play(String type, String fileName) {
        if("mp4".equals(type)){
            advancedMediaPlayer.playMp4(fileName);
        } else if("vlc".equals(type)){
            advancedMediaPlayer.playVlc(fileName);
        }
    }
}
