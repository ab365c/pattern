package com.example.pattern.chain;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.chain.ConsoleLogger
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/21 14:28
 * @version:1.0
 */
public class ConsoleLogger extends AbstractLogger {
    public ConsoleLogger(int level) {
        this.level = level;
    }

    @Override
    protected void write(String message) {
        System.out.println("Standard Console::Logger: " + message);
    }
}
