package com.example.pattern.chain;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.chain.FileLogger
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/21 14:31
 * @version:1.0
 */
public class FileLogger extends AbstractLogger {

    public FileLogger(int level) {
        this.level = level;
    }

    @Override
    protected void write(String message) {
        System.out.println("File Console::Logger: " + message);
    }
}
