package com.example.pattern.chain;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.chain.AbstractLogger
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/21 14:23
 * @version:1.0
 */
public abstract class AbstractLogger {
    public static int INFO = 1;
    public static int WARN = 2;
    public static int ERROR = 3;

    protected int level;

    protected AbstractLogger nextLogger;

    public void setNextLogger(AbstractLogger nextLogger) {
        this.nextLogger = nextLogger;
    }

    public void logMessage(int level, String message) {
        if(this.level <= level){
            write(message);
        }

        if(nextLogger!=null){
            nextLogger.logMessage(level, message);
        }
    }

    abstract protected void write(String message);
}
