package com.example.pattern.chain;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.chain.ErrorLogger
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/21 14:29
 * @version:1.0
 */
public class ErrorLogger extends AbstractLogger {

    public ErrorLogger(int level) {
        this.level = level;
    }

    @Override
    protected void write(String message) {
        System.out.println("Error Console::Logger: " + message);
    }
}
