package com.example.pattern.chain;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.chain.ChainPatternDemo
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/21 14:22
 * @version:1.0
 */
public class ChainPatternDemo {
    public static void main(String[] args) {
        AbstractLogger loggerChain = getChainOfLoggers();
        loggerChain.logMessage(AbstractLogger.INFO, "This is an information!");

        loggerChain.logMessage(AbstractLogger.WARN, "This is a warn level information!");

        loggerChain.logMessage(AbstractLogger.ERROR, "This is an error level information!");
    }

    private static AbstractLogger getChainOfLoggers() {
        AbstractLogger errorLogger = new ErrorLogger(AbstractLogger.ERROR);
        AbstractLogger fileLogger = new FileLogger(AbstractLogger.WARN);
        AbstractLogger consoleLogger = new ConsoleLogger(AbstractLogger.INFO);
        errorLogger.setNextLogger(fileLogger);
        fileLogger.setNextLogger(consoleLogger);
        return errorLogger;
    }
}
