package com.example.pattern.singleton.exercise;

public class HungrySingleton {
    private static HungrySingleton instance = new HungrySingleton();
    public static HungrySingleton getInstance(){
        return instance;
    }
}
