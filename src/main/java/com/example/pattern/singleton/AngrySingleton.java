package com.example.pattern.singleton;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.singleton.AngrySingleton
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/6/29 19:47
 * @version:1.0
 */
public class AngrySingleton {
    private static AngrySingleton angrySingleton = new AngrySingleton();

    private AngrySingleton() {
    }

    /**
     * 饿汉式
     * @return
     */
    public static AngrySingleton getInstance() {
        return angrySingleton;
    }
}
