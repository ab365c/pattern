package com.example.pattern.singleton;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.singleton.StaticSingleton
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/6/29 19:54
 * @version:1.0
 */
public class StaticSingleton {

    private StaticSingleton() {
    }

    public static final StaticSingleton getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private static class SingletonHolder {
        private static final StaticSingleton INSTANCE = new StaticSingleton();
    }
}
