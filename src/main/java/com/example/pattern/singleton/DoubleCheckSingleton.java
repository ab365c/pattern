package com.example.pattern.singleton;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.singleton.DoubleCheckSingleton
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/6/29 19:49
 * @version:1.0
 */
public class DoubleCheckSingleton {
    private static DoubleCheckSingleton instance;

    private DoubleCheckSingleton() {
    }

    public static DoubleCheckSingleton getInstance() {
        if(instance == null){
            synchronized (DoubleCheckSingleton.class){
                if(instance == null) {
                    instance = new DoubleCheckSingleton();
                }
            }
        }
        return instance;
    }
}
