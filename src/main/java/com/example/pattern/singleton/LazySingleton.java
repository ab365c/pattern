package com.example.pattern.singleton;

/**
 * @description: 懒汉式，非线程安全
 * @project: pattern
 * @name: com.example.pattern.singleton.LazyFactory
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/6/29 19:42
 * @version:1.0
 */
public class LazySingleton {

    private static LazySingleton instance;

    /**
     * 线程安全可以添加synchronized
     *
     * @return
     */
    public static LazySingleton getInstance() {
        if (instance == null) {
            instance = new LazySingleton();
        }
        return instance;
    }

    private LazySingleton() {

    }

}
