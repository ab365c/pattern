package com.example.pattern.singleton;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.singleton.EnumSingleton
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/6/29 19:57
 * @version:1.0
 */
public enum EnumSingleton {
    INSTANCE;

    //添加自己需要的操作
    public void singletonOperation() {

    }
}
