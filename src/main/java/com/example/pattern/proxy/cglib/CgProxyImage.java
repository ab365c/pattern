package com.example.pattern.proxy.cglib;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.proxy.cglib.CgProxyImage
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/13 17:53
 * @version:1.0
 */
public class CgProxyImage implements MethodInterceptor {

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        System.out.println("begin time -----> "+ System.currentTimeMillis());
        Object o1 = methodProxy.invokeSuper(o, objects);
        System.out.println("end time -----> "+ System.currentTimeMillis());
        return o1;
    }
}
