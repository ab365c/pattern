package com.example.pattern.proxy.dynamic;

import com.example.pattern.proxy.Image;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.proxy.dynamic.DynamicProxyImage
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/13 17:05
 * @version:1.0
 */
public class DynamicProxyImage implements InvocationHandler {
    private Image image;

    public DynamicProxyImage(Image image) {
        this.image = image;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("--------------begin-------------");
        Object invoke = method.invoke(image, args);
        System.out.println("--------------end-------------");
        return invoke;
    }
}
