package com.example.pattern.proxy;

import com.example.pattern.proxy.cglib.CgProxyImage;
import net.sf.cglib.proxy.Enhancer;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.proxy.ProxyPatternDemo
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/6/30 15:32
 * @version:1.0
 */
public class ProxyPatternDemo {
    public static void main(String[] args) {
        // 静态代理
/*        Image image = new ProxyImage("test01.jpg");
        image.display();
        System.out.println();
        image.display();*/

        // JDK动态代理
/*        Image dynamicImage = new RealImage("test02.jpg");
        InvocationHandler imageProxy = new DynamicProxyImage(dynamicImage);
        Image proxyInstance = (Image) Proxy.newProxyInstance(imageProxy.getClass().getClassLoader(),
                image.getClass().getInterfaces(), imageProxy);
        proxyInstance.display();*/

        // cglib动态代理
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(RealImage.class);
        enhancer.setCallback(new CgProxyImage());
        Class[] classes = {String.class};
        String[] params = {"test03.jpg"};
        Image realImage = (RealImage) enhancer.create(classes, params);
        realImage.display("test03.jpg");
    }
}
