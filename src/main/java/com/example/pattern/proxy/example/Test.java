package com.example.pattern.proxy.example;

public class Test {
    public static void main(String[] args) {
        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        LettingAgent lettingAgent = new LettingAgent();
        ICustomer customerLiProxy = lettingAgent.getInstance(new CustomerLi());
        customerLiProxy.findHouse();

        ICustomer customerWangProxy = lettingAgent.getInstance(new CustomerWang());
        customerWangProxy.findHouse();

        ICustomer customerZhangProxy = lettingAgent.getInstance(new CustomerZhang());
        customerZhangProxy.findHouse();

    }
}
