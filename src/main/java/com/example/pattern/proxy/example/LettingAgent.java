package com.example.pattern.proxy.example;

import lombok.AllArgsConstructor;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class LettingAgent implements InvocationHandler {

    private ICustomer target;

    public ICustomer getInstance(ICustomer target){
        this.target = target;
        Class<?> clazz =  target.getClass();
        return (ICustomer) Proxy.newProxyInstance(clazz.getClassLoader(),clazz.getInterfaces(),this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("--------------begin-------------");
        Object invoke = method.invoke(target, args);
        System.out.println("--------------end-------------");
        return invoke;
    }
}
