package com.example.pattern.proxy.example;

public class CustomerWang implements ICustomer{
    @Override
    public void findHouse() {
        System.out.println("三室一厅二卫");
        System.out.println("价格：65万");
        System.out.println("面积：120平以上");
    }
}
