package com.example.pattern.proxy.example;

import java.util.List;

public class CustomerLi implements ICustomer{
    @Override
    public void findHouse() {
        System.out.println("三室一厅一卫");
        System.out.println("价格：55万");
        System.out.println("面积：110平以上");
    }
}
