package com.example.pattern.proxy.example;

import java.util.List;

public interface ICustomer {

    void findHouse();

}
