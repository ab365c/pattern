package com.example.pattern.proxy.example;

public class CustomerZhang implements ICustomer{
    @Override
    public void findHouse() {
        System.out.println("二室一厅一卫");
        System.out.println("价格：45万");
        System.out.println("面积：90平以上");
    }
}
