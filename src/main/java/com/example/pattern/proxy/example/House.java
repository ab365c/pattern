package com.example.pattern.proxy.example;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class House {
    private String address;
    private BigDecimal area;
    private BigDecimal price;
    private String comment;

}
