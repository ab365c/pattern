package com.example.pattern.proxy;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.proxy.Image
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/13 16:48
 * @version:1.0
 */
public interface Image {
    void display(String fileName);

    void loadFromDisk(String fileName);
}
