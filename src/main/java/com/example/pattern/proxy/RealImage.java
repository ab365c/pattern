package com.example.pattern.proxy;

import com.example.pattern.proxy.Image;
import lombok.Data;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.proxy.RealImage
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/13 16:48
 * @version:1.0
 */
@Data
public class RealImage implements Image {

    private String fileName;

    public RealImage(String fileName) {
        this.fileName = fileName;
        loadFromDisk(fileName);
    }

    @Override
    public void display(String fileName) {
        System.out.println("Displaying " + fileName);
    }
    @Override
    public void loadFromDisk (String fileName) {
        System.out.println("loading " + fileName);
    }
}
