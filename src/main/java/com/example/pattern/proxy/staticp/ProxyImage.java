package com.example.pattern.proxy.staticp;

import com.example.pattern.proxy.Image;
import com.example.pattern.proxy.RealImage;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.proxy.staticp.ProxyImage
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/13 16:48
 * @version:1.0
 */
public class ProxyImage implements Image {
    private RealImage realImage;
    private String fileName;

    public ProxyImage(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void display(String fileName) {
        if (realImage == null) {
            realImage = new RealImage(fileName);
        }
        realImage.display(fileName);
    }

    @Override
    public void loadFromDisk(String fileName) {

    }
}
