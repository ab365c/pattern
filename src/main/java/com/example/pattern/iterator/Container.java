package com.example.pattern.iterator;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.iterator.Container
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/21 14:58
 * @version:1.0
 */
public interface Container {
    Iterator getIterator();
}
