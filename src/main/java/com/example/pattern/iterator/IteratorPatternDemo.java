package com.example.pattern.iterator;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.iterator.IteratorPatternDemo
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/21 14:53
 * @version:1.0
 */
public class IteratorPatternDemo {
    public static void main(String[] args) {
        NameRepository nameRepository = new NameRepository();
        for(Iterator iterator = nameRepository.getIterator(); iterator.hasNext();){
            String name = (String) iterator.next();
            System.out.println("name:" +name);
        }
    }
}
