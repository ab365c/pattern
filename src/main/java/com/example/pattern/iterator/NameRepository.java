package com.example.pattern.iterator;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.iterator.NameRepository
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/21 14:59
 * @version:1.0
 */
public class NameRepository implements Container {

    public String[] names = {"Robert", "John", "Jolie", "Lora"};

    @Override
    public Iterator getIterator() {
        return new NameIterator();
    }

    private class NameIterator implements Iterator {

        int index;

        @Override
        public boolean hasNext() {
            if (index < names.length) {
                return true;
            }
            return false;
        }

        @Override
        public Object next() {
            if (this.hasNext()) {
                return names[index++];
            }
            return null;
        }
    }
}
