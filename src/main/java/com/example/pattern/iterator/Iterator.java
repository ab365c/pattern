package com.example.pattern.iterator;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.iterator.Iterator
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/21 14:57
 * @version:1.0
 */
public interface Iterator {
    boolean hasNext();
    Object next();
}
