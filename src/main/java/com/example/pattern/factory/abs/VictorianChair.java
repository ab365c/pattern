package com.example.pattern.factory.abs;

public class VictorianChair implements Chair {
    @Override
    public void hasLegs() {
        System.out.println();
    }

    @Override
    public void sitOn() {

    }
}
