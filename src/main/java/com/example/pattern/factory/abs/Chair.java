package com.example.pattern.factory.abs;

public interface Chair extends Furniture{
    void hasLegs();
    void sitOn();
}
