package com.example.pattern.factory.simple;

public interface Sender {

    void send();
}
