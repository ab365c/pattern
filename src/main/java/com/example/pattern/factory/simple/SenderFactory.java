package com.example.pattern.factory.simple;

public class SenderFactory {
    public static Sender getSender(String type){
        if("email".equals(type)){
            return new EmailSender();
        } else if("sms".equals(type)) {
            return new SmsSender();
        }
        return null;
    }

    public static Sender produceSmsSender(){
        return new SmsSender();
    }

    public static Sender produceEmailSender(){
        return new EmailSender();
    }

}
