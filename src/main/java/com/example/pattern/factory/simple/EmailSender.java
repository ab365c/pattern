package com.example.pattern.factory.simple;

public class EmailSender implements Sender{
    @Override
    public void send() {
        System.out.println("电子邮件发送");
    }
}
