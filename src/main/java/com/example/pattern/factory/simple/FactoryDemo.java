package com.example.pattern.factory.simple;



/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.factory.FactoryMain
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/6/28 9:46
 * @version:1.0
 */
public class FactoryDemo {
    public static void main(String[] args) {
//        SenderFactory.getSender("email").send();
//        SenderFactory.getSender("sms").send();
//        SenderFactory.produceEmailSender().send();
//        SenderFactory.produceSmsSender().send();
    }
}
