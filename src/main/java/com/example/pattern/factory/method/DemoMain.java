package com.example.pattern.factory.method;

public class DemoMain {
    public static void main(String[] args) {
        Provider provider = new SmsProvider();
        provider.produce().send();
        provider = new EmailProvider();
        provider.produce().send();
    }
}
