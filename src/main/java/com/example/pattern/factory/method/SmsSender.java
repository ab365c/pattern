package com.example.pattern.factory.method;

public class SmsSender implements Sender{
    @Override
    public void send() {
        System.out.println("短信发送");
    }
}
