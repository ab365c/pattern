package com.example.pattern.factory.method;

public interface Sender {
    public void send();
}
