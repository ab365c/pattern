package com.example.pattern.factory.method;

public class EmailSender implements Sender{
    @Override
    public void send() {
        System.out.println("邮件发送");
    }
}
