package com.example.pattern.factory.method;

public interface Provider {
    public Sender produce();
}

class SmsProvider implements Provider{

    @Override
    public Sender produce() {
        return new SmsSender();
    }
}

class EmailProvider implements Provider{

    @Override
    public Sender produce() {
        return new EmailSender();
    }
}