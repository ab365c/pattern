package com.example.pattern.builder;

/**
 * @description: 创建一个表示食物条目和食物包装的接口
 * @project: pattern
 * @name: com.example.pattern.builder.Item
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/6/29 20:33
 * @version:1.0
 */
public interface Item {
    String name();
    Packing packing();
    float price();

}
