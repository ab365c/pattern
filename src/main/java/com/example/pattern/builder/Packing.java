package com.example.pattern.builder;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.builder.Packing
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/6/29 20:34
 * @version:1.0
 */
public interface Packing {
    /**
     * 包装
     * @return
     */
    public String pack();
}
