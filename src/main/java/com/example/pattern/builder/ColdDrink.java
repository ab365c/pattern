package com.example.pattern.builder;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.builder.ColdDrink
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/6/30 15:06
 * @version:1.0
 */
public abstract class ColdDrink implements Item {

    @Override
    public Packing packing() {
        return new Bottle();
    }

    @Override
    public abstract float price();
}
