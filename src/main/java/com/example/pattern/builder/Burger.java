package com.example.pattern.builder;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.builder.Burger
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/6/30 15:05
 * @version:1.0
 */
public abstract class Burger implements Item {

    @Override
    public Packing packing() {
        return new Wrapper();
    }

    @Override
    public abstract float price();
}
