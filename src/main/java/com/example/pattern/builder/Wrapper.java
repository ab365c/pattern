package com.example.pattern.builder;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.builder.Wrapper
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/6/30 14:56
 * @version:1.0
 */
public class Wrapper implements Packing {
    @Override
    public String pack() {
        return "wrapper";
    }
}
