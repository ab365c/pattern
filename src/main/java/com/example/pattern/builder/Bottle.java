package com.example.pattern.builder;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.builder.Bottle
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/6/30 15:03
 * @version:1.0
 */
public class Bottle implements Packing {
    @Override
    public String pack() {
        return "bottle";
    }
}
