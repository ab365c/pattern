package com.example.pattern.builder;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.builder.Pepsi
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/6/30 15:09
 * @version:1.0
 */
public class Pepsi extends ColdDrink {
    @Override
    public String name() {
        return "Pepsi";
    }

    @Override
    public float price() {
        return 35.0f;
    }
}
