package com.example.pattern.builder;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.builder.ChickenBurger
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/6/30 15:08
 * @version:1.0
 */
public class ChickenBurger extends Burger {
    @Override
    public String name() {
        return "Chicken Burger";
    }

    @Override
    public float price() {
        return 50.5f;
    }
}
