package com.example.pattern.command;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.command.Order
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/22 9:25
 * @version:1.0
 */
public interface Order {
    void execute();
}
