package com.example.pattern.command;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.command.ButStock
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/22 9:28
 * @version:1.0
 */
public class BuyStock implements Order{

    private Stock stock;

    public BuyStock(Stock stock) {
        this.stock = stock;
    }

    @Override
    public void execute() {
        stock.buy();
    }
}
