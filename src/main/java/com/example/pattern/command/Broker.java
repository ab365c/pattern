package com.example.pattern.command;

import java.util.ArrayList;
import java.util.List;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.command.Broker
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/22 9:30
 * @version:1.0
 */
public class Broker {
    private List<Order> orderList = new ArrayList<>();
    public void takeOrder(Order order){
        orderList.add(order);
    }

    public void placeOrders(){
        for (Order order:
             orderList) {

        }
    }

}
