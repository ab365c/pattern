package com.example.pattern.command;

/**
 * @description:
 * @project: pattern
 * @name: com.example.pattern.command.Stock
 * @author: 李家东
 * @email: lijiadong@esvtek.com
 * @createTime: 2020/7/22 9:26
 * @version:1.0
 */
public class Stock {

    private String name = "ABC";
    private int quantity = 10;

    public void buy() {
        System.out.println("Stock [ Name: " + name + ", Quantity: " + quantity + " ] bought");
    }

    public void sell() {
        System.out.println("Stock [ Name: " + name + ", Quantity: " + quantity + " ] sold");
    }
}
