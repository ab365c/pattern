package com.example.dto;

import lombok.Data;

@Data
public class ListNode {
    int val;
    ListNode next;

    public ListNode() {
    }

    public ListNode(int val) {
        this.val = val;
    }

    public ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }

    @Override
    public String toString(){
        ListNode dummyNode = new ListNode();
        dummyNode.next = this;
        ListNode preNode = dummyNode;
        String str = "";
        while(preNode.next != null) {
            preNode = preNode.next;
            str = str+preNode.val+",";
        }
        return str;
    }
}
