package com.example.dto;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * @author Administrator
 * @description
 * @date 2022/3/1 10:18
 */
public class DrawingPanel {
    private JFrame frame;
    private JPanel panel;
    private Graphics g;

    public DrawingPanel(int width, int height) {
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        g = image.getGraphics();
        g.setColor(Color.BLACK);
        JLabel label = new JLabel();
        label.setIcon(new ImageIcon(image));
        panel = new JPanel(new FlowLayout());
        panel.setBackground(Color.WHITE);
        panel.setPreferredSize(new Dimension(width, height));
        panel.add(label);
        frame = new JFrame("Drawing Panel");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.add(panel);
        frame.pack();
        frame.setVisible(true);

    }

    public Graphics getGraphics() {
        return g;
    }


    public void setBackground(Color c) {
        panel.setBackground(c);
    }

    public void setVisible(boolean visible) {
        frame.setVisible(visible);
    }
}