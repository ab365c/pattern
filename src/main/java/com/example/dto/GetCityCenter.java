package com.example.dto;

import com.alibaba.fastjson.JSONObject;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Administrator
 * @description
 * @date 2022/3/22 16:10
 */
public class GetCityCenter {

    public static void main(String[] args) throws IOException {
        BigDecimal maxLongitude = new BigDecimal("139.970756504097");
        BigDecimal minLongitude = new BigDecimal("70.0186796595312");
        BigDecimal maxLatitude = new BigDecimal("56.3833398450615");
        BigDecimal minLatitude = new BigDecimal("14.9813367469598");

        BigDecimal cellLon = maxLongitude.subtract(minLongitude);
        BigDecimal cellLat = maxLatitude.subtract(minLatitude);

        // 经度切分 487；纬度切分 367
        BigDecimal divideLon = cellLon.divide(new BigDecimal("487"), 12, BigDecimal.ROUND_HALF_UP);
        BigDecimal divideLat = cellLat.divide(new BigDecimal("367"), 12, BigDecimal.ROUND_HALF_UP);

        BigDecimal lon = minLongitude;
        BigDecimal lat = minLatitude;

        BigDecimal two = new BigDecimal("2");

        Date startTime = new Date();
        System.out.println(startTime.getTime());

        String fileName = "theFirstLine_21_60.txt";
        File file = new File(fileName);
        if (file.exists()) {
            file.delete();
            file = new File(fileName);
        }
        FileWriter fw = new FileWriter(file);

        for (int i = 21; i <= 60; i++) {
            lat = minLatitude.add(divideLat.divide(two, 12, BigDecimal.ROUND_HALF_UP)).add(divideLat.multiply(new BigDecimal(i - 1)));
            for (int j = 1; j <= 487; j++) {
                lon = minLongitude.add(divideLon.divide(two, 12, BigDecimal.ROUND_HALF_UP)).add(divideLon.multiply(new BigDecimal(j - 1)));
                String str = test(lon.toString(), lat.toString());
                JSONObject jsonObject = JSONObject.parseObject(str);

                JSONObject resJson = new JSONObject();
                resJson.put("lon", lon.toString());
                resJson.put("lat", lat.toString());
                JSONObject addressCom = jsonObject.getJSONObject("result").getJSONObject("addressComponent");
                resJson.put("province", addressCom.getString("province"));
                resJson.put("city", addressCom.getString("city"));
                resJson.put("district", addressCom.getString("district"));
                resJson.put("adcode", addressCom.getString("adcode"));
                fw.write(resJson.toJSONString());
                fw.write("\r\n");
            }
            fw.flush();
        }

        fw.flush();
        fw.close();
        long cost = System.currentTimeMillis() - startTime.getTime();
        System.out.println("耗时：" + cost + "ms");
    }

    public static String test(String lon, String lat) throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url("https://api.map.baidu.com/reverse_geocoding/v3/?ak=wHvQDPdodRdIuBsVnINS0dolYTl4ML1I&output=json&coordtype=bd09ll&location=" + lat + "," + lon)
                .method("GET", null)
                .addHeader("Cookie", "BAIDUID=AF6655F809831D49548F341906203AA8:FG=1")
                .build();
        Response response = client.newCall(request).execute();
        String string = response.body().string();
        System.out.println(string);
        return string;
    }


}
